#!/bin/bash

# from https://stackoverflow.com/questions/6141581/detect-python-version-in-shell-script

ret=`python -c 'import sys; print("%i" % (sys.hexversion<0x03000000))'`
if [ $ret -eq 0 ]; then
    # python 3
    python -m http.server 8080 > /dev/null 2>&1 &
else 
   # python 2
    python -m SimpleHTTPServer 8080 > /dev/null 2>&1 &
fi

ngrok http 5555


