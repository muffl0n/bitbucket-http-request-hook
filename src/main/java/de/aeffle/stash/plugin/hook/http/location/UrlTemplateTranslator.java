/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.http.location;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.NoSuchCommitException;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;

import de.aeffle.stash.plugin.hook.HttpRequestPostReceiveHook;
import org.apache.commons.lang.text.StrSubstitutor;

import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.user.ApplicationUser;
import org.apache.log4j.Logger;


public class UrlTemplateTranslator {
    private static final Logger log = Logger.getLogger(UrlTemplateTranslator.class);
    private final Map<String, String> translationMap;

    public UrlTemplateTranslator(ApplicationPropertiesService applicationPropertiesService,
                                 AuthenticationContext authenticationContext,
                                 CommitService commitService,
                                 RepositoryHookContext repositoryHookContext,
                                 RefChange refChange) {

        translationMap = new HashMap<String, String>();

        try {
            if (applicationPropertiesService != null) {
                addApplicationPropertiesContext(applicationPropertiesService);
            }

            if (authenticationContext != null) {
                addAuthenticationContext(authenticationContext);
            }

            if (repositoryHookContext != null) {
                addRepositoryHookContext(repositoryHookContext);
            }

            if (refChange != null) {
                addRefChange(refChange);
            }

            if (repositoryHookContext != null && refChange != null && commitService != null) {
                addCommitMessage(commitService, repositoryHookContext, refChange);
                addTimestamps(commitService, repositoryHookContext, refChange);
            }
        }
        catch (Exception e){
            log.error("Problem when creating the translatior: ", e);
        }
    }

    private void addApplicationPropertiesContext(ApplicationPropertiesService applicationPropertiesService) {
        URI baseUrl = applicationPropertiesService.getBaseUrl();

        addTranslation("baseUrl", baseUrl.toString());
        addTranslation("baseUrl.protocol", baseUrl.getScheme());
        addTranslation("baseUrl.host", baseUrl.getHost());
        addTranslation("baseUrl.port", ((Integer) baseUrl.getPort()).toString());
        addTranslation("baseUrl.path", baseUrl.getPath());
    }

    public void addRepositoryHookContext(RepositoryHookContext repositoryHookContext) {
        Repository repository = repositoryHookContext.getRepository();
        addTranslation("repository.id", Integer.toString(repository.getId()));
        addTranslation("repository.name", urlEncode(repository.getName()));
        addTranslation("repository.slug", repository.getSlug());
        addTranslation("project.name", urlEncode(repository.getProject().getName()));
        addTranslation("project.key", repository.getProject().getKey());
        addTranslation("project.key.lower", repository.getProject().getKey().toLowerCase());
    }

    public void addAuthenticationContext(AuthenticationContext authenticationContext) {
        ApplicationUser user = authenticationContext.getCurrentUser();
        addTranslation("user.name", user.getName());
        addTranslation("user.slug", user.getSlug());
        addTranslation("user.displayName", urlEncode(user.getDisplayName()));
        addTranslation("user.email", user.getEmailAddress());
    }

    public void addRefChange(RefChange refChange) {
        addTranslation("refChange.refId", refChange.getRef().getId());
        addTranslation("refChange.fromHash", refChange.getFromHash());
        addTranslation("refChange.toHash", refChange.getToHash());
        addTranslation("refChange.type", refChange.getType().toString());

        String branchOrTagName = refChange.getRef().getId().replaceAll("^refs/(tags|heads)/", "");
        addTranslation("refChange.name", urlEncode(branchOrTagName));
    }

    public void addCommitMessage(CommitService commitService,
                                 RepositoryHookContext repositoryHookContext,
                                 RefChange refChange)
    {
        Repository repo = repositoryHookContext.getRepository();
        String latestCommitId = refChange.getToHash();

        if (latestCommitId != null && !latestCommitId.equals("0000000000000000000000000000000000000000")) {
            CommitRequest commitRequest = new CommitRequest.Builder(repo, latestCommitId).build();
            Commit latestCommit = commitService.getCommit(commitRequest);

            String latestCommitMessage = urlEncode(latestCommit.getMessage());
            addTranslation("refChange.latest.message", latestCommitMessage);
        } else {
            addTranslation("refChange.latest.message", "");
        }
    }


    public void addTimestamps(CommitService commitService,
                              RepositoryHookContext repositoryHookContext,
                              RefChange refChange)
    {
        Repository repo = repositoryHookContext.getRepository();
        String latestCommitId = refChange.getToHash();

        final Date latestTimestamp;
        final Date latestAuthorTimestamp;

        if (latestCommitId != null && !latestCommitId.equals("0000000000000000000000000000000000000000")) {
            CommitRequest commitRequest = new CommitRequest.Builder(repo, latestCommitId).build();
            Commit latestCommit = commitService.getCommit(commitRequest);

            latestTimestamp = latestCommit.getAuthorTimestamp();
            latestAuthorTimestamp = latestCommit.getAuthorTimestamp();
        } else {
            latestTimestamp = new Date(0L);
            latestAuthorTimestamp = new Date(0L);
        }

        addTranslation("refChange.latest.timeStamp.unix", getUnixString(latestTimestamp));
        addTranslation("refChange.latest.timeStamp.iso", getIsoString(latestTimestamp));
        addTranslation("refChange.latest.timeStamp.rfc", getRfcString(latestTimestamp));

        addTranslation("refChange.latest.authorTimeStamp.unix", getUnixString(latestAuthorTimestamp));
        addTranslation("refChange.latest.authorTimeStamp.iso", getIsoString(latestAuthorTimestamp));
        addTranslation("refChange.latest.authorTimeStamp.rfc", getRfcString(latestAuthorTimestamp));

        /**
         * TODO: only available for bitbucket version > 5.0
        Date latestCommiterTimestamp = latestCommit.getCommitterTimestamp();
        addTranslation("refChange.latest.commitTimeStamp.unix", getUnixString(latestCommiterTimestamp));
        addTranslation("refChange.latest.commitTimeStamp.iso", getIsoString(latestCommiterTimestamp));
        addTranslation("refChange.latest.commitTimeStamp.rfc", getRfcString(latestCommiterTimestamp));
        */
    }


    public String getUnixString(Date date) {
        return String.valueOf(date.getTime());
    }

    public String getIsoString(Date date) {
        return DateTimeFormatter.ISO_OFFSET_DATE_TIME
                .withZone(ZoneId.systemDefault())
                .format(date.toInstant());
    }

    public String getRfcString(Date date) {
        return DateTimeFormatter.RFC_1123_DATE_TIME
                .withZone(ZoneId.systemDefault())
                .format(date.toInstant());
    }


    private String urlEncode(String raw) {
        if (raw != null) {
            try {
                return URLEncoder.encode(raw, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private void addTranslation(String key, String value) {
        translationMap.put(key, value);
    }

    public HttpLocation translate(HttpLocation httpLocationUntranslated) {
        StrSubstitutor strSubstitutor = new StrSubstitutor(translationMap);
        String url = strSubstitutor.replace(httpLocationUntranslated.getUrlTemplate());
        String postData = strSubstitutor.replace(httpLocationUntranslated.getPostDataTemplate());

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .urlTemplate(httpLocationUntranslated.getUrlTemplate())
                .url(url)
                .sslValidationDisabled(httpLocationUntranslated.isSslValidationDisabled())
                .httpMethod(httpLocationUntranslated.getHttpMethod())
                .postContentType(httpLocationUntranslated.getPostContentType())
                .postDataTemplate(httpLocationUntranslated.getPostDataTemplate())
                .postData(postData)
                .authEnabled(httpLocationUntranslated.isAuthEnabled())
                .user(httpLocationUntranslated.getUser())
                .pass(httpLocationUntranslated.getPass())
                .branchFilter(httpLocationUntranslated.getBranchFilter())
                .tagFilter(httpLocationUntranslated.getTagFilter())
                .userFilter(httpLocationUntranslated.getUserFilter())
                .build();

        return httpLocationTranslated;
    }

}