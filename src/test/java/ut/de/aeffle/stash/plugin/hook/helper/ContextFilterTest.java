/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.helper;

import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.auth.AuthenticationContext;
import de.aeffle.stash.plugin.hook.filter.ContextFilter;
import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import org.junit.Before;
import org.junit.Test;
import ut.de.aeffle.stash.plugin.hook.testHelpers.RefChangeMockFactory;
import ut.de.aeffle.stash.plugin.hook.testHelpers.AuthenticationContextMockFactory;

import static org.assertj.core.api.Assertions.assertThat;


public class ContextFilterTest {

	private final AuthenticationContextMockFactory authenticationContextFactory = new AuthenticationContextMockFactory();
	private final RefChangeMockFactory refChangeMockFactory = new RefChangeMockFactory();

    @Before
    public void beforeTestClearSettings() {
    	authenticationContextFactory.clear();
    	refChangeMockFactory.clear();
	}

    @Test
    public void testEmptyFiltersShouldReturnMatch() {
        authenticationContextFactory.setSlug("john.doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .branchFilter("")
                .tagFilter("")
                .userFilter("")
                .build();

        boolean result = contextFilter.isMatching(httpLocationTranslated);

        assertThat(result).isTrue();
    }

    @Test
    public void testMatchingBranchFilterQueryShouldReturnMatch() {
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .branchFilter(".*")
                .tagFilter("")
                .userFilter("")
                .build();

        boolean result = contextFilter.isMatching(httpLocationTranslated);

        assertThat(result).isTrue();
    }

    @Test
    public void testMatchingRegExShouldMatch() {
        authenticationContextFactory.setSlug("john.doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .branchFilter("*")
                .tagFilter("")
                .userFilter("")
                .build();

        boolean result = contextFilter.isMatching(httpLocationTranslated);

        assertThat(result).isTrue();
    }

    @Test
    public void testMatchingTagsShouldMatch() {
        authenticationContextFactory.setSlug("john.doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/tags/special-tag");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .branchFilter("")
                .tagFilter("^special.*")
                .userFilter("")
                .build();

        boolean result = contextFilter.isMatching(httpLocationTranslated);

        assertThat(result).isTrue();
    }

    @Test
    public void testNotMatchingBranchFilterShouldNotMatch() {
        authenticationContextFactory.setSlug("john.doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .branchFilter("test")
                .tagFilter("")
                .userFilter("")
                .build();

        boolean result = contextFilter.isMatching(httpLocationTranslated);

        assertThat(result).isFalse();
    }

    @Test
    public void testNotMatchingTagsShouldNotMatch() {
        authenticationContextFactory.setSlug("john.doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/tags/special-tag");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .branchFilter("")
                .tagFilter("^special$")
                .userFilter("")
                .build();

        boolean result = contextFilter.isMatching(httpLocationTranslated);

        assertThat(result).isFalse();
    }

    @Test
    public void testNotMatchingUserFilterShouldNotMatch() {
        authenticationContextFactory.setSlug("john.doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .branchFilter(".*")
                .tagFilter("")
                .userFilter("jane.doe")
                .build();

        boolean result = contextFilter.isMatching(httpLocationTranslated);

        assertThat(result).isFalse();
    }
}
